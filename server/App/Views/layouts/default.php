<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $title ?? 'default' ?></title>
</head>
<body>
    <header>
        <div>
            <div>
                <a href="/">home</a>
            </div>
            <div>
                <div>
                    <?php $user_login  ?>
                </div>
<!--                //или иф если зашел , то это убрать и тд-->
<!--                     -->
                <?= $user_login->first_name ?>
                <a href="/login">Sign in</a>
            </div>
        </div>
    </header>

<?= $content ?>
</body>
</html>

<?php
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>


<div class=".form-group text-center">
    <form action="user/update" class="form-control">
        <div class="form mb-3">
            <label for="name" class="col-sm-2 col-form-label">Enter Your Name
                <input type="text" class="input form-control" value="<?= $user ?>">
            </label>
        </div>
        <div class="form mb-3">
            <label for="email" class="col-sm-2 col-form-label">Enter Your Email
                <input type="email" class="input form-control">
            </label>
        </div>
        <div class="form mb-3">
            <label for="password" class="col-sm-2 col-form-label">Enter Your  new password
                <input type="password" class="input form-control">
            </label>
        </div>
        <div class="form mb-3">
            <label for="password" class="col-sm-2 col-form-label">Confirm your new password
                <input type="password" class="input form-control">
            </label>
        </div>
        <div>
            <select class="form mb-3 form-select" aria-label="Default select example" name="gender">
                <option selected>Select gender status</option>
                <option value="0">Not known</option>
                <option value="1">Male</option>
                <option value="2">Female</option>
                <option value="9">Not applicable</option>
            </select>
        </div>
        <div>
            <button type="submit" class="btn btn-success mb-3">Send</button>
        </div>
        <div>
            <button type="reset" class="btn btn-danger mb-3">Reset</button>
        </div>
    </form>
</div>


<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $('form').on('submit', function (e) {
        e.preventDefault()
        let $form = $(this),
            password = $form.find("input[name='password']").val(),
            email = $form.find("input[name='email']").val(),
            gender = $form.find("select[name='gender']").val(),
            url = $form.attr("action");
        $.post(
            url,
            {gender: gender, email: email, password: password, submit: 'submit'},
            function (data, status) {
                $('.success').text("Status: " + status);
            }
        );
        $('input').eq(0).val('');
        $('input').eq(1).val('');



    })
</script>
</body>
</html>



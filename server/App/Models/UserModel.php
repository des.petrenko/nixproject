<?php

namespace App\Models;

use Petrenko\Framework\Models\Model;

class UserModel extends Model
{
    protected string $table = 'users';

    protected bool $created_at = true;
    protected bool $updated_at = true;

//    protected array $closeSelection = ['password', 'id'];

    /**
     * @param $array
     */
    public static function createUser($array)
    {


        $userModel = new self;
        $arr = [];
        $arr['name'] = $array['name'];
        $arr['email'] = $array['email'];
        $arr['password'] = password_hash($array['password'], PASSWORD_BCRYPT);
        $arr['gender'] = $array['gender'];

        $userModel->insert($array);


    }

    /**
     * @param $id
     * @return array
     */
    public static function getOne($id): array
    {
        $userModel = new self;
        return $userModel->where('id', '=', $id);
    }

    public static function updated(array $array, int $id)
    {
        $userModel = new self;
        $arr = [];

        if (!empty($arr['name']) && $arr['name'] != $array['name']) {
            $arr['name'] = $array['name'];
        }
        if (!empty($arr['password']) && $arr['password'] != $array['password']) {
            $arr['password'] = password_hash($array['password'], PASSWORD_BCRYPT);
        }
        if (!empty($arr['email']) && $arr['password'] != $array['password']) {
            $arr['email'] = $array['email'];
        }
        if (strlen($array['gender']) == 1) {
            $arr['gender'] = $array['gender'];
        }

        $userModel->update($arr);
    }


    public function deleteUser(int $id)
    {
        $userModel = new UserModel();
        $userModel->delete($id);
    }

    public function sortUser()
    {
        $userModel = new self;

    }

    public function updateU($arraySearch, $arraySet)
    {

    }

}

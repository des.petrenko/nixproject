<?php

use App\Controllers\MainController;
use App\Controllers\UserController;
use Petrenko\Framework\Route;

Route::add('/', [MainController::class, 'index']);
Route::add('^$', [MainController::class, 'index']);

Route::add('/login', [UserController::class, 'login']);
Route::add('/register', [UserController::class, 'register']);

Route::add('/user', [UserController::class, 'index']);

Route::add('/user/create', [UserController::class, 'create']);
Route::add('/user/update', [UserController::class, 'update']);
Route::add('/user/delete', [UserController::class, 'delete']);
//сессия. обьект юзера

//$user = $_SESSION['user'];
//$user ['id'];
Route::add('/list', [\App\Controllers\ArticleController::class, 'list']);
//динамический роут
Route::add('/list{$id}', [\App\Controllers\ArticleController::class, 'list']);
/**
 * 1. URL  /all article for authors
 *
 *
 *
 */
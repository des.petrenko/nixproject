<?php

namespace Petrenko\Framework\Controllers;

use Petrenko\Framework\Views\View;

/**
 * Class Controller
 * author
 */

abstract class Controller
{
    /**
     * @var array
     */
    public array $route = [];

    /**
     * @param $route
     */
    public function __construct($route)
    {
        $this->route = $route;
    }

    /**
     * @param string $view
     * @param array $data
     * @return bool
     */
    public function view(string $view, array $data = []): bool
    {
        $viewObject = new View($this->route, $view);
        $viewObject->render($data);
        return true;
    }

}

//{
//    /**
//     * @var array
//     */
//    public array $route = [];
//    /**
//     * @var string
//     */
//    public string $viewPath;
//    /**
//     * @var array
//     */
//    public array $data = [];
//
//
//    /**
//     * @param $route
//     */
//    public function __construct($route)
//    {
//        $this->route = $route;
//
//    }
//
//    /**
//     * @param string $viewPath
//     * @param array $data
//     * @return bool
//     */
//    public function view(string $viewPath, array $data = []): bool
//    {
//
//
//        $viewObject = new View($this->route, $viewPath);
//        if (!empty($this->data)) {
//            $data = array_merge($data, $this->data);
//        }
//        $viewObject->render($data);
//
//
//        return true;
//    }
//
//    public function set($data)
//    {
//        $this->data = $data;
//    }
//
//}
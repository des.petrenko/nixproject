<?php

namespace Petrenko\Framework;

use ZabaraIndastry\Logger\Logger;
class Route
{
    protected static array $routes = [];
    protected static array $route = [];

    /**
     * @param string $url
     * @param array $route
     * @return void
     */
    public static function add(string $url, array $route = []): void
    {
        self::$routes[$url] = $route;
    }
    /**
     * @param string $url
     * @return void
     */
    public static function dispatch(string $url): void
    {

        if (self::matchRoute($url)) {
            $controller = self::$route[0];
            if (class_exists($controller)) {
                $obj = new $controller(self::$route);
                $action = self::$route[1];
                if (method_exists($obj, $action)) {
                    $obj->$action();
                } else {
                    echo 'method ' . $action . ' not found';
                }
            } else {
                echo 'Controller' . $controller . 'not found';
            }
        } else {
            http_response_code(404);
            include '404.html';
        }
    }

    /**
     * @param string $url
     * @return bool
     */
    public static function matchRoute(string $url): bool
    {
        foreach (self::$routes as $pattern => $route) {
            if ($pattern == $url) {
                self::$route = $route;
                return true;
            }
        }
        return false;
    }
}

///**
// * Class Route
// *  @ author Denys Petrenko < Des . Petrenko @ gmail . com >
// */
//class Route
//{
//    /**include all routes
//     * @var array
//     */
//    protected static array $routes = [];
//    /** route in work
//     * @var array
//     */
//    protected static array $route = [];
//
//    /** register new route
//     * @param string $url
//     * @param array $route
//     * @return void
//     */
//    public static function add(string $url, array $route = []): void
//    {
//        self::$routes[$url] = $route;
//
//    }
//
//
//    /** create object class
//     * @param string $url
//     * @return void
//     */
//    public static function dispatch(string $url): void
//    {
//        if (self::matchRoute($url)) {
//            $controller = self::$route[0];
//            if (class_exists($controller)) {
//                $cObj = new $controller(self::$route);
//                $action = self::$route[1];
//                if (method_exists($cObj, $action)) {
////                    $data = 1;
//
//                    $cObj->$action();
//                } else {
//                    echo 'Метод  ' . $action . 'не существует';
//                    Logger::log($action);
//                }
//            } else {
//
//                echo 'Controller' . $controller . ' не найден';
//                Logger::log($controller);
//            }
//
//        } else {
//            http_response_code(404);
//
//            include '404.html';
//        }
//    }
//
//    /** check register route
//     * @param string $url
//     * @return bool
//     */
//    public static function matchRoute(string $url): bool
//    {
//        //сделать исключение
////        $pos1 = stripos($url, '$');
////        if ($pos1 !==false) {
////            return true;
////        }
//        foreach (self::$routes as $pattern => $route) {
//            if ($pattern == $url) {
//                self::$route = $route;
//                return true;
//            }
//        }
//        return false;
//    }
//
//}
//

<?php
namespace ZabaraIndastry\Logger;
class Singleton
{
    private static array $instances = [];

    protected function __construct()
    {

    }

    protected function __clone(): void
    {
    }

    public static function getInstance()
    {
        $class = static::class;
        if (!isset(self::$instances[$class])) {
            self::$instances[$class] = new static();
        }
        return self::$instances[$class];
    }
}
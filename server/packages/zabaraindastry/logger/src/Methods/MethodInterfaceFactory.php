<?php
namespace ZabaraIndastry\Logger\Methods;

interface MethodInterfaceFactory
{
    public function writeLog($level, $message);
}
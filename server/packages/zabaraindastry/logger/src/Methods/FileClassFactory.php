<?php
namespace ZabaraIndastry\Logger\Methods;

use ZabaraIndastry\Logger\Singleton;

class FileClassFactory extends Singleton implements MethodInterfaceFactory
{
    private $handle;

    protected function __construct()
    {
        $file = '/storage/logs' . date("Y-m-d") .  'txt';
        $this->handle = fopen($_SERVER["DOCUMENT_ROOT"] . $file, 'a+');
    }

//    public static function log($message)
//    {
//
//    }
//
//    public function write($message, $level)
//    {
//        $date = date("Y-m-d G:i:s");
//        $str = $date . ' ' . print_r($message, true) . "\r\n\ ";
//        fwrite($this->handle, $str);
//    }

    public function writeLog($level, $message)
    {
        $date = date("Y-m-d");
        $str = $level . '|' . $date . ' ' . print_r($message, true) . "\r\n";
        fwrite($this->handle, $str);
    }
}
<?php

namespace ZabaraIndastry\Logger;
use ZabaraIndastry\Logger\Methods\DbClassFactory;
class Logger extends Singleton
{
    public static function log($context)
    {
        self::method(require 'config/log.php')->writeLog(__FUNCTION__, $context);
        $log = static::getInstance();

    }

    public static function error($context)
    {
        self::method(require 'config/log.php')->writeLog(__FUNCTION__, $context);

    }

    public static function method($class)
    {
        $listenerClass = 'ZabaraIndastry\\Logger\\Methods\\' . ucfirst($class) . 'ClassFactory';

        if (class_exists($listenerClass)) {
            $log = static::getInstance();
            return $listenerClass::getInstance();
        } else {
            echo $listenerClass;
            exit('This class not exist' . ucfirst($class) . 'ClassFactory');
        }


    }
}